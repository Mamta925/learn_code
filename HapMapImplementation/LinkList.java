public class LinkList <K,V> {
	int hash;
	K key;
	V value;
	LinkList<K,V> next;  
	
	public void setAllValue(K key, V value, int hash,LinkList next )  {
		this.key = key;  
		this.value = value;  
		this.hash = hash;  
		this.next = next; 
	}
	public void setNode(LinkList next) {
		this.next = next;
	}
	public LinkList getNode() {
		return this.next;
	}
	public K getKey() {
		return this.key;
	}
	public V getValue() {
		return this.value;
	}
	public void SetValue(V value) {
	 this.value = value;
	}
}
