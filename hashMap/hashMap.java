import java.util.*;
import java.io.*;
public class CheckPopuplarity  {

    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	String mostLikedSport;
	int maxNumberOfLikes;

	public void getInput() {
	   try{
	     int totalPeople = Integer.parseInt(bufferedReader.readLine());
	     HashSet<String> setOfPeople = new HashSet<>();
	     HashMap<String,Integer> mapOfNameAndSport = new HashMap<>();
		
	     mostLikedSport="";
	     maxNumberOfLikes=-1;
	     getHighestPopularity(mapOfNameAndSport, setOfPeople,totalPeople);
	
	   }
	   catch(IOException e) {
         e.printStackTrace();
       }
	}
	public void getHighestPopularity(HashMap<String,Integer> mapOfNameAndSport,HashSet<String> setOfPeople,int totalPeople) {
	 try{
		for(int indexOfPerson = 0;indexOfPerson<totalPeople; indexOfPerson++) {
            String arrOfNameAndSport[] = bufferedReader.readLine().split(" ");
			if(!setOfPeople.contains(arrOfNameAndSport[0])) {
				if(mapOfNameAndSport.get(arrOfNameAndSport[1]) == null) {
					mapOfNameAndSport.put(arrOfNameAndSport[1],1);
					if(maxNumberOfLikes < 1) {
						maxNumberOfLikes = 1;
						mostLikedSport = arrOfNameAndSport[1];
					}
				} else {
					int noOfLikes = mapOfNameAndSport.get(arrOfNameAndSport[1]);
					if((noOfLikes+1) == maxNumberOfLikes) { 
                        if((mostLikedSport.compareTo(arrOfNameAndSport[1])) > 0) {
						mostLikedSport = arrOfNameAndSport[1];
                        }
					} else	if((noOfLikes+1) > maxNumberOfLikes) {
						maxNumberOfLikes += 1;
						mostLikedSport = arrOfNameAndSport[1];
					}	
					mapOfNameAndSport.put(arrOfNameAndSport[1],noOfLikes+1);
				}
				setOfPeople.add(arrOfNameAndSport[0]);
			}
			
	    }
	    prinPopularity(mapOfNameAndSport);
	    } catch(IOException e) {
        e.printStackTrace();
      }
	}

	public void prinPopularity(HashMap<String,Integer> mapOfNameAndSport) {
		System.out.println(mostLikedSport);
		if(mapOfNameAndSport.get("football") == null) {
            System.out.println(0);
		} else {
           System.out.println(mapOfNameAndSport.get("football"));
		}
	}

    public static void main(String args[]) {
		CheckPopuplarity statistics = new CheckPopuplarity();  
		statistics.getInput();

	} 
}