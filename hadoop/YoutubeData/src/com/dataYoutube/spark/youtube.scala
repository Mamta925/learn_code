package com.dataYoutube.spark
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
object YouTube {
    def main(args: Array[String]) {
     
     //Create conf object
     val conf = new SparkConf()
     .setAppName("YouTube")
     
     //create spark context object
     val sc = new SparkContext(conf)
    //Check whether sufficient params are supplied
     if (args.length < 2) {
     println("Usage: ScalaYoutube <input> <output>")
     System.exit(1)
     }
      val textFile = sc.textFile(args(0));
      val counts = textFile.map(line=>{var YoutubeRecord = ""; val temp=line.split("\t"); ;if(temp.length >= 3) {YoutubeRecord=temp(3)};YoutubeRecord})
      val test=counts.map ( x => (x,1) )
      val res=test.reduceByKey(_+_).map(item => item.swap).sortByKey(false).take(5)
     
     
   
     //Save the result
     test.saveAsTextFile(args(1))
    //stop the spark context
     sc.stop
     }
}