#include<bits/stdc++.h>
using namespace std;

int numberOfSpiders,selectedSpiders,powerofSpiders[100000];
queue <int>queueOfSpiders;


void takeInput()
{
    cin>>numberOfSpiders;
    cin>>selectedSpiders;
    for(int powerIndex=0;powerIndex<numberOfSpiders;powerIndex++){
        cin>>powerofSpiders[powerIndex];
        queueOfSpiders.push(powerIndex);
    }
}

void dequeAndEnqueSpider(){
    int index=0;
    while(index<selectedSpiders){
        int maxPower=-1,maxPowerIndex,arr[selectedSpiders+1];
        int queueSize = queueOfSpiders.size();
        for(int index1=0;index1<min(selectedSpiders,queueSize);index1++)
        {          
             arr[index1]=queueOfSpiders.front();
             queueOfSpiders.pop();
             if(powerofSpiders[arr[index1]]>maxPower)
             {
				 maxPower=powerofSpiders[arr[index1]];
                 maxPowerIndex=arr[index1];
            }
        }
        cout<<maxPowerIndex+1<<" ";
        for(int index1=0;index1<min(selectedSpiders,queueSize);index1++)
        {
            if(arr[index1]==maxPowerIndex)
            continue;
            else
            {
                if(powerofSpiders[arr[index1]])
                powerofSpiders[arr[index1]]--;
                queueOfSpiders.push(arr[index1]);
           }
        }
    index++;
    }  
}

int main()
{
  
	takeInput();
	dequeAndEnqueSpider();
	return 0;
}