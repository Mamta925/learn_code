export interface IMatrix {

  addAlternateNumber(inputArray: number[], next: any): any;

  processInput(inputString: string, next: any): any;

  addNumbersOfArray(inputArray: number[], next: any): any;
}