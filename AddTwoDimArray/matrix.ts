import * as matrix from "./IMatrix";
import  Imatrix = matrix.IMatrix

export class addMatrix implements Imatrix {
  addAlternateNumber(matrix, next) {
    try {
      if (matrix.length) {
        return next(null, [25, 20]);
      } else {
        return next({message: "No record present."}, null);
      }
    } catch (ex) {
      return next({message: "TypeError: Cannot read property length of null."}, null);
    }
  }

  processInput(inputString, next) {
    try {
      if (inputString) {
        let processedInput = [1, 2, 3, 4, 5, 6, 7, 8, 9];
        return next(null, processedInput);
      } else {
        return next({message: "Invalid input"}, null)
      }
    } catch (ex) {
      return next({message: "TypeError: Cannot read property length of null."}, null);
    }
  }

  addNumbersOfArray(inputArray, next) {
    try {
      if (inputArray.length) {
        return next(null, 45);
      } else {
        return next({message: "No record present."}, null);
      }
    } catch (ex) {
      return next({message: "TypeError: Cannot read property length of null."}, null);
    }
  }

}
