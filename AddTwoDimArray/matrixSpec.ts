/// <reference path="mocha.d.ts" />
import "mocha";
import {expect} from "chai";
import * as matrix from "./matrix";

let testData = require("./testData.json");
let addAlternateNumberOfMatrix = new matrix.addMatrix();

describe("should add alternate number", () => {

  before(function (done: Function) {
    done();
  });

  it("should convert string into array addMatrix() => processInput()=>", () => {
    addAlternateNumberOfMatrix.processInput(testData.inputString, function (err, data) {
      expect(err).to.be.null;
      expect(data).not.to.be.null;
      expect(data).not.to.be.empty;
      expect(data).instanceof(Array);
      expect(data).to.have.length(9);
      expect(data[0]).to.be.equal(testData.outputArray[0]);
    })
  });
  it("should give an error as invalid input addMatrix() => processInput => ", () => {
    addAlternateNumberOfMatrix.processInput(null, function (err, data) {
      expect(data).to.be.null;
      expect(err).not.to.be.null;
      expect(err.message).to.eql("Invalid input");
    })
  });
  it("should add alternate numbers addMatrix() => addAlternateNumber => ", () => {
    addAlternateNumberOfMatrix.addAlternateNumber(testData.outputArray, function (err, data) {
      expect(err).to.be.null;
      expect(data).not.to.be.null;
      expect(data).not.to.be.empty;
      expect(data).instanceof(Array);
      expect(data).to.have.length(2);
      expect(data[0]).to.be.equal(25);
      expect(data[1]).to.be.equal(20);
    })
  });

  it("should give an  error No record present. addMatrix() => addAlternateNumber => ", () => {
    let inputArray = [];
    addAlternateNumberOfMatrix.addAlternateNumber(inputArray, function (err, data) {
      expect(data).to.be.null;
      expect(err).not.to.be.null;
      expect(err.message).to.eql("No record present.");
    })
  });
  it("should return exception addMatrix() => addAlternateNumber => ", () => {
    addAlternateNumberOfMatrix.addAlternateNumber(null, function (err, data) {
      expect(data).to.be.null;
      expect(err).not.to.be.null;
      expect(err.message).to.eql("TypeError: Cannot read property length of null.");
    })
  });
  it("should add numbers of array addMatrix() => addNumbersOfArray => ", () => {
    addAlternateNumberOfMatrix.addNumbersOfArray(testData.outputArray, function (err, data) {
      expect(err).to.be.null;
      expect(data).not.to.be.null;
      expect(data).equals(45);
    })
  });
  it("should return err addMatrix() => addNumbersOfArray => ", () => {
    addAlternateNumberOfMatrix.addNumbersOfArray(null, function (err, data) {
      expect(data).to.be.null;
      expect(err).not.to.be.null;
      expect(err.message).to.eql("TypeError: Cannot read property length of null.");
    })
  });

  after(function (done: Function) {
    done();
  });
});
