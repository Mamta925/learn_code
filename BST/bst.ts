class Node {
  constructor(data) {
    this.value = data;
    this.left = null;
    this.right = null;
  }
}

class BinarySearchTree {
  constructor() {
    // root of a binary seach tree
    this.root = null;
  }

  isBST(node) {
    if (!node) {
      return true;
    }

    if (node.left != null && node.left.value > node.value) {
      return false;
    }

    if (node.right != null && node.right.value < node.value) {
      return false;
    }

    if (!isBST(node.left) || !isBST(node.right)) {
      return false;
    }

    return true;
  }

  insert(value) {
    var root = this.root;

    if (!root) {
      this.root = new Node(value);
      return;
    }

    var currentNode = root;
    var newNode = new Node(value);

    while (currentNode) {
      if (value < currentNode.value) {
        if (!currentNode.left) {
          currentNode.left = newNode;
          break;
        }
        else {
          currentNode = currentNode.left;
        }
      }
      else {
        if (!currentNode.right) {
          currentNode.right = newNode;
          break;
        }
        else {
          currentNode = currentNode.right;
        }
      }
    }

  }

  getRoot() {
    return this.root;
  }

  getHeightOfBst(node) {
    if (!node) {
      return -1;
    }
    var left = this.getHeightOfBst(node.left);
    var right = this.getHeightOfBst(node.right);
    return Math.max(left, right) + 1;
  };

  getHeight(node) {
    if (!node) {
      node = this.getRoot();
    }
    return this.getHeightOfBst(node);
  };

}

var bst = new BinarySearchTree();
bst.insert(3);
bst.insert(2);
bst.insert(4);
bst.insert(1);
bst.insert(5);
console.log(bst.getHeight());
module.exports = {
  Node: Node,
  BinarySearchTree: BinarySearchTree
};