#include<stdio.h>
#include<stdlib.h>

struct node {
    int key;
    struct node *left, *right;
};

struct node *createNode(int item) {
    struct node *tempNode = (struct node *)malloc(sizeof(struct node));
    tempNode->key = item;
    tempNode->left = tempNode->right = NULL;
    return tempNode;
}

int findHeight(struct node * node) {
    if (node == NULL) {
        return 0;
    }
    int lefthNode = findHeight(node->left);
    int righthNode = findHeight(node->right);  
    if (lefthNode > righthNode) {
        return (lefthNode + 1);
    } else {
        return (righthNode + 1);
    }
}

struct node* insert(struct node* root, int key) {
    if (root == NULL) return createNode(key);
    if (key <= root->key)
	   root->left = insert(root->left, key);
    else if (key > root->key)
	   root->right = insert(root->right, key);
    return root;
}

struct node *makeBst(struct node* root,int numberOfElements) {
    int index=0, key;
    for(int index =0;index<numberOfElements;index++){
        scanf("%d",&key);
        if(root == NULL) {
            root = insert(root, key);
        } else {
            insert(root, key);  
        }
    }
    return root;
}

void printHeight(int heightOfBst) {
     printf("%d",heightOfBst);
}

int main()
{
    struct node *root = NULL;
    int numberOfElements,heightOfBst;
    scanf("%d",&numberOfElements);
    root = makeBst(root,numberOfElements);
    heightOfBst = findHeight(root);
    printHeight(heightOfBst);
    return 0;
}
