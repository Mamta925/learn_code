/// <reference path="mocha.d.ts" />
import "mocha";
import {expect} from "chai";
import * as bst from "./bst";

var binarySearchTree = new bst.BinarySearchTree();


describe("get all users of an organization  by organization id", () => {

  before(function (done) {
    done();
  });
  it("root should be null", () => {
    let root = binarySearchTree.getRoot();
    expect(root).equals(null);

  });
  it("should insert data in root", () => {

    binarySearchTree.insert(3);
    expect(binarySearchTree).not.equals(null);
    expect(binarySearchTree.root.value).equals(3);
    expect(binarySearchTree.root.value).not.to.be.a("string");
    expect(binarySearchTree.root.left).equals(null);
    expect(binarySearchTree.root.right).equals(null);
  });
  it("should insert data in root's right", () => {

    binarySearchTree.insert(4);
    expect(binarySearchTree.root.right.value).equals(4);
    expect(binarySearchTree.root.right).not.equals(null);
    expect(binarySearchTree.root.left).equals(null);
    expect(binarySearchTree.root.right.value).not.to.be.a("string");
  });
  it("should insert data in root's left", () => {

    binarySearchTree.insert(2);
    expect(binarySearchTree.root.left.value).equals(2);
    expect(binarySearchTree.root.left).not.equals(null);
    expect(binarySearchTree.root.left.value).not.to.be.a("string");
  });
  it("should get height 1", () => {

    expect(binarySearchTree.getHeight()).equals(1);

  });
  it("should get height 2", () => {

    binarySearchTree.insert(5);
    expect(binarySearchTree.getHeight()).equals(2);
    binarySearchTree.insert(1);
    expect(binarySearchTree.getHeight()).equals(2);

  });
});
