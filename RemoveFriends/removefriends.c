#include <stdio.h>
#include<stdlib.h>
struct friend
{
   int popularity;
   struct friend *next;  
};
typedef struct friend friend;

void printRemainingFriends(friend **firstNode) {
    friend *temp = *firstNode;
    while(temp != NULL){
        printf("%d ",temp->popularity);
        temp = temp->next;
    }
    printf("\n");
}

void removeFriends(friend **firstNode,int noOfFriends,int noOfFriendsToBeDel) {
    friend *curNode = *firstNode;
    friend *trackfirstNode = *firstNode;
    friend *preNode = NULL;
	while(curNode->next!=NULL && curNode != NULL) {	   
        if(noOfFriendsToBeDel>0) {    
            if(curNode->popularity < curNode->next->popularity ) {
                if(trackfirstNode == curNode) {
                    trackfirstNode = curNode->next;
                    curNode=trackfirstNode;
                } else {     
                    preNode->next = curNode->next;
                    curNode = preNode->next;
                } 
            noOfFriendsToBeDel--;	
            } else {
                preNode = curNode;
                curNode = curNode->next;
            }
    } else 
        break;
    if(curNode->next == NULL && noOfFriendsToBeDel>0) 
      curNode = trackfirstNode;
    }
    printRemainingFriends(&trackfirstNode);  	   
}

friend* createNode(int popularity) {
    friend *newnode = (friend *)malloc(sizeof(friend));
    if (newnode == NULL) {
        printf("\nMemory was not allocated");
        return 0;
    }
    else {
        newnode->popularity = popularity;
        newnode->next = NULL;
        return newnode;
    }
}

void inputFriendList(int numberOfTestcase) {
    int popularityOfFriends,noOfFriends,noOfFriendsToBeDel,index;
    friend *newNode,*tempNode;
	friend *firstNode =NULL;
    while(numberOfTestcase) { 
        scanf("%d %d",&noOfFriends,&noOfFriendsToBeDel);
        for(index=0;index<noOfFriends;index++) {
            scanf("%d",&popularityOfFriends);
            newNode = createNode(popularityOfFriends);
            if(firstNode == NULL) {
                firstNode = newNode;
            } else {
                tempNode = firstNode;
                while(tempNode->next!=NULL) {
                    tempNode=tempNode->next;
                }
                tempNode->next = newNode;
            }
        }
        removeFriends(&firstNode,noOfFriends,noOfFriendsToBeDel);
	    firstNode =NULL,
        numberOfTestcase--;
    }
}
	
int main() { 
    int numberOfTestcase;
    scanf("%d",&numberOfTestcase);
    inputFriendList(numberOfTestcase);
}

