#include<stdio.h>
#include<stdlib.h>

struct friend {
    int popularity ;
    struct friend *next;
};
typedef struct friend friend;

friend* createFriendNode(int  popularity ) {
    friend* newFriend = (friend*) malloc(sizeof(friend));
    newFriend->popularity = popularity;
    newFriend->next = NULL;
    return newFriend;
}

friend* addNewFriend(friend* topFriend, friend* newFriend) {
   if(topFriend) { 
     newFriend->next = topFriend;
   }
   return newFriend;
}

friend* deleteFriend(friend* topFriend) {
    if(topFriend == NULL)
   {
      return topFriend; 
   }
    friend *deleteFriendNode = topFriend->next;
    free(topFriend);
    return deleteFriendNode;
}

void printFriendPopularity(friend* head) {
    if(head == NULL)
        return;
    printFriendPopularity(head->next);
    printf("%d ", head->popularity);
}

int main() {
    int testCase,numberOfFriend,friendstoDelete,index,count,popularity,deleteCounter;
    friend* topFriend = NULL, *newFriend;
    scanf("%d", &testCase);
    while(testCase--) {
        scanf("%d %d", &numberOfFriend, &friendstoDelete);
        deleteCounter = friendstoDelete;
        topFriend = NULL;
        for(count=0; count<numberOfFriend; count++) {
            scanf("%d", &popularity);
            newFriend = createFriendNode(popularity);
             while(topFriend!=NULL && topFriend->popularity < popularity && deleteCounter) {
                    deleteCounter--;
                    topFriend = deleteFriend(topFriend);      
              }
                topFriend = addNewFriend(topFriend, newFriend);        
         }
        printFriendPopularity(topFriend);
        printf("\n");
        } 
    return 0;
}
